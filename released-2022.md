# Released
| Wednesday | Units | Upgrade | Vision Cards | 3-star Espers | Raid |  Challenge | Story | Event / JP Date |
| ---      | ---   | ---     | ---          | ---           | --- | --- | --- | --- |
| 5 Jan 22 | [Jeume](https://players.wotvffbe.com/1475/) | Jeume | - | - | - | - | - | 1 Sep 21 | - |
| 12 Jan 22 | - | [Lu'Cia, Lilyth, Sosha](https://players.wotvffbe.com/1483/) | [Ahriman Gold(UR), Immortal Lich(MR)](https://players.wotvffbe.com/1489/) | Ahriman Gold, Lich (ตามการ์ด), Chocobo | [Elafikelas (มี Rare Raid Boss เอาไว้เก็บการ์ด MR)](https://players.wotvffbe.com/1488/) | - | 242 | 8 Sep 21 | - |
| 19 Jan 22 | [Golbeza] | [Golbeza], Cecil, Rosa, Kain, Lucio | - | Demon Wall | - | - | - | 15 Sep 21 | FFIV |
| 26 Jan 22 | Rayches (Water Selection Quest) | Rayches, [Ildyra, Niv'Lu, Mia](https://players.wotvffbe.com/1483/) | [A Loyal Companion](http://wotvffbe.gamea.co/c/kji3xuqt) | Bloody Moon | - | - | 243 | 22 Sep 21 | - |
| 2 Feb 22 | Moore the Merrier | Moore the Merrier | Ferver of the Festival | - | - | - | - | R: 29 Sep 21 | - |
| 9 Feb 22 | [Cetia (WotC)](https://players.wotvffbe.com/1555/) | Cetia,[Skahal,Mariale,Loreila](https://players.wotvffbe.com/1574/) | - | - | - | - | 251, AS12 | 8 Oct 21 | - |
| 16 Feb 22 | - | - | [Ancient Giant Dragon](https://players.wotvffbe.com/1575/) | - | - | - | AS13 | 15 Oct 21 | Halloween |
| 23 Feb 22 | Rerun: Summer units | Luathar, Livialle, Cadia, 2nd Master Ability (Mont, Sterne, Oelde, Robb) | - | - | - | - | 252 | 22 Oct 21 | - |
| 2 Mar 22 | Halloween Rerun | - | - | Marilith | - | - | - | - | - |
| 9 Mar 22 | [Ranan (WotC) Cost 100](https://players.wotvffbe.com/1624/) | [Howlett, Margritte, Lorenzo](https://players.wotvffbe.com/1643/) | - | - | - | - | 253 | 1 Nov 21 | - |
| 16 Mar 22 | FF XIV Rerun | Thancred, Y'shtola, ~~[Howlett, Margritte, Lorenzo](https://players.wotvffbe.com/1643/)~~ | [Europa, Breath Blooming](https://players.wotvffbe.com/1640/), Scion of Twilight | - | Kraken | - | ~~253~~ | 8 Nov 21 | - |
| 23 Mar 22 | Terra, Locke, Celes (FF VI) | Terra, Locke, Celes | - | - | FF XIV raid revival | - | AS14 | 14 Nov 21 | FF VI |
| 30 Mar 22 | ~~Celes (FF VI)~~ | ~~Celes, Thancred, Y'shtola~~ | Omen (FF VI), Dark Odin | Dark Odin | - | - | 261 | 22 Nov 21 | FF VI, FF XIV Rerun |
| 6 Apr 22 | [Velis (WotC) Cost 100](https://players.wotvffbe.com/1714/) | Velis (WotC) | - | - | ~~FF XIV raid revival~~ | - | - | 1 Dec 21 | - |
| 13 Apr 22 | - | Elsirelle,Ahlim,Grace | [Trembling Heart](https://players.wotvffbe.com/1730/) | - | [Trial of Reckoning (FF VI)](https://players.wotvffbe.com/1722/) for [Terra's Ribbon](https://wotv-calc.com/JP/equipment/tina-no-ribon) | - | 262 | 8 Dec 21 | - |
| 20 Apr 22 | Esther (Global Original Unit), ~~Luartha (Winter)~~ | Esther, ~~Luartha (Winter)~~ | The Dreamiest of Eggs | - | - | - | AS5 | 15 Dec 21 | - |
| 27 Apr 22 | Joker, Queen (Free Unit), Violet (Persona 5) | Joker, Queen, Violet | ~~P5 Card (UR)~~ | - | - | - | 263 | 22 Dec 21 | Persona 5 (til 31 Jan 22) |
| 4 May 22 | [Astrius (WotC) Cost 100](https://players.wotvffbe.com/1807/), ~~Violet (Persona 5)~~ | Astrius (WotC), ~~Violet~~ | Changing the World Takes Heart (P5 UR), ~~[Swearing to the first Asahi](https://players.wotvffbe.com/1807/)~~ | - | - | - | - | 29 Dec 21 (1 Jan 22) |  |
| 11 May | - | [Garvall, Ellshra](https://players.wotvffbe.com/1835/) | [Faint Torchlight](https://players.wotvffbe.com/1834/), [Oath of a New Dawn](https://players.wotvffbe.com/1807/), [Those Who Defy Reality (Raid)](https://players.wotvffbe.com/1837/) | - | [P5R](https://players.wotvffbe.com/1836/) | - | [271](https://players.wotvffbe.com/1833/) | 5 Jan ( 7 Jan ) |  |
| 18 May | [Resnick](https://players.wotvffbe.com/1854/) | Resnick | - | - | - | [The Crucible of the Warrior - Lucio's Sword Quest](https://players.wotvffbe.com/1853/) | - | 12 Jan ( 17 Jan ) |  |
| 25 May | - | [Sakura, Rhaldor](https://players.wotvffbe.com/1874/) | [Salire vs Rayches, Round 30](https://players.wotvffbe.com/1876/) [[ i ]](https://wotv-calc.com/JP/card/dai-30-ji-horun-tousou) | - | - | [Guild Raid](https://players.wotvffbe.com/1870/) | 272 | 24/1 ) |  | 
| 1 Jun | [Little Leela the Bold Cost 100](https://players.wotvffbe.com/1899/) | Little Leela the Bold | -          | -           | - | - | - | 1/2 ) | - |
| 8 Jun | [Cyrelle (MR)](https://players.wotvffbe.com/1906/) [(Acquisition Quest)](https://players.wotvffbe.com/1907/) | Cyrelle (MR) | [Anima](https://players.wotvffbe.com/1906/) | - | [Porcelain Tower](https://players.wotvffbe.com/1904/) | - | [273](https://players.wotvffbe.com/1905/) | 8/2 ) |  |
| 15/6 | [Ildyra (Sweetheart)](https://twitter.com/WOTV_FFBE/status/1492060337457090569) | Ildyra (Sweetheart) | - | - | - | - | - | 15/2 | Valentine, JP Delivery |
| 22/6 | - | - | [Valefor](https://players.wotvffbe.com/1949/) | - | - | - | 281 | 22/2 | FF X Rerun |
| 29/6 | WotC, GLEX Re-issue | - | - | - | [Trial of Reckoning FF X](https://players.wotvffbe.com/1980/) | - | -| - | - |
| 6/7 | [Sadali Cost 100](https://players.wotvffbe.com/1965/) | Sadali | - | - | - | - | -| 1/3 | - |
| 13/7 | - | Vadim | [Culmination](https://players.wotvffbe.com/1991/) | - | Guild Raid, ~~[Trial of Reckoning FF X](https://players.wotvffbe.com/1980/)~~ | - | [282](https://players.wotvffbe.com/1990/) | 9/3 | - |
| 20/7 | [Eliza, Ranell](https://players.wotvffbe.com/2015/) | Eliza, Ranell | - | - | - | - | - | 16/3 | - |
| 27/7 | Lara Croft (Reissue) | - | Tomb Raider (Reissue), [Fruit of Life](https://players.wotvffbe.com/2031/) | - | - | [Random Battle Quest](https://players.wotvffbe.com/2030/), [Big Beast](https://players.wotvffbe.com/2029/) | [283](https://players.wotvffbe.com/2027/) | 23/3 | - |
| 3/8 | [Dark Fina (UR)](https://players.wotvffbe.com/2087/), [Macleod (MR - Free)](https://players.wotvffbe.com/2088/) | Dark Fina, Macleod (MR) | - | - | - | - | - | 15/4 | - |
| 10/8 | - | (2nd Master) Rain, Fina, Yuni | [Another Me](https://players.wotvffbe.com/2115/) | - | Porcelain Tower | [Challenge](https://players.wotvffbe.com/2113/) | 291 ~~[292](https://players.wotvffbe.com/2112/)~~ | 22/4 | - |
| 17/8 | Jayden (Summer) | Jayden (Summer), (2nd Master)Lasswell, Kitone (Summer), Lilyth (Summer), Kilphe (Summer), Elsirelle (Summer) | Stag of the Shining Summer, Friend or Foe? | - | - | - | - | - | - |
| 24/8 | - | - | ~~Starlight Elena Card~~, [Blood on the Ice - Free from Raid](https://players.wotvffbe.com/2074/) | - | [Demon Chimera](https://players.wotvffbe.com/2069/) | - | 292 ~~[291](https://players.wotvffbe.com/2073/)~~ | 8/4 | - |
| 31/8 | - | - | Rise of the Immortal | - | - | - | - | - | - |
| 7/9 | [Macherie (Queen of Hourne)](https://players.wotvffbe.com/2156/) | Macherie (Queen of Hourne) | - | - | - | - | - | 1/5 | - |
| 14/9 |  - | - | [Sheratan](https://players.wotvffbe.com/2178/) | Sheratan | [Porcelain Tower](https://players.wotvffbe.com/2177/) | - | [293](https://players.wotvffbe.com/2176/) | 9/5 | - |
| 21/9 | [Lightning, Hope](https://players.wotvffbe.com/2260/), [Snow](https://players.wotvffbe.com/2279/) | Lightning, Hope, Snow | - | - | - | - | - | 14/5 | 2.5Y, FFXIII |
| 28/9 | [FFT Rerun](https://players.wotvffbe.com/2280/) | FFT Units | [Lightning, Hope VC](https://players.wotvffbe.com/2260/), [Snow Card](https://players.wotvffbe.com/2279/) | - | - | [Midlight's Deep](https://players.wotvffbe.com/2282/) | [2-10-1](https://players.wotvffbe.com/2276/) | 23/5 | [FF 35th Anniversary](https://players.wotvffbe.com/2284/) |
| 5/10 | [Glaciela, Flag Bearer](https://players.wotvffbe.com/2299/) | [Glaciela, Flag Bearer](https://players.wotvffbe.com/2299/) | - | - | - | - | - | 1/6 | - |
| 12/10 | - | - | [Dark Tetra Sylphid](https://players.wotvffbe.com/2319/) | - | - | ToR FFXIII | [2-10-2](https://players.wotvffbe.com/2318/) | 8/6 | - |
| 19/10 | [Lu'Cia Halloween](https://players.wotvffbe.com/2857/), [Chunak, Rivelka](https://players.wotvffbe.com/2336/) | Chunak, Rivelka | - | - | - | - | Background Repeat | 15/6 | - |
| 26/10 | - | - | [Intersecting Destinies](https://players.wotvffbe.com/2349/), [Nightmare Halloween](https://players.wotvffbe.com/2857/) | - | - | FFXIII Guild Raid | [2-10-3](https://players.wotvffbe.com/2346/) | 22/6 | ~[Feature: Background Repeat](https://players.wotvffbe.com/2352/)~ |
| 2/11 | [Alaya Rundall](https://players.wotvffbe.com/2397/) | Alaya Rundall | - | - | - | - | - | 1/7 | - |
| 9/11 | - | - | [Peaceful Days, A Meal to Remember](https://players.wotvffbe.com/2413/) | - | [Tetra Sylphid](https://players.wotvffbe.com/2410/) | - | [2-10-4](https://players.wotvffbe.com/2412/) | 8/7 | - |
| 16/11 | [Eurel](https://players.wotvffbe.com/2475/) | Eurel | [Beyond the Flames](https://players.wotvffbe.com/2488/) | - | - | - | - | 15/7 | - |
| 23/11 | Silvie | Silvie | A Party for All, ~~[Beyond the Flames](https://players.wotvffbe.com/2488/)~~ | - | - | [Selection Quest Wind+](https://players.wotvffbe.com/2486/), [Ice Brand](https://players.wotvffbe.com/2485/) | [Interlude - Book of Despair - Section 1](https://players.wotvffbe.com/2484/) | 25/7 | - |
| 30/11 | [Cherise, Lissette](https://players.wotvffbe.com/2544/) | Cherise, Lissette | - | - | - | - | - | 17/8 | - |
| 7/12 | [Helena (Swimsuit)](https://players.wotvffbe.com/2499/) | Helena (Swimsuit) | - | - | - | - | - | 1/8 | - |
| 14/12 | - | (2nd Master) Howlet, Tyytas, Chel | [Dark Ramuh](https://players.wotvffbe.com/2519/) | - | [Porcelain Tower](https://players.wotvffbe.com/2517/) | [Raid Boss Revival](https://players.wotvffbe.com/2518/) | [Interlude - Book of Despair - Section 2](https://players.wotvffbe.com/2516/) | 9/8 | - |
| 21/12 | Raviesse (Winter) | Raviesse (Winter) | Wishing on a Winter Night | - | - | - | - | - | - |
| 28/12 | Shadowlynx on Ice ~~[Zoma, Slime](https://players.wotvffbe.com/2640/)~~ | Shadowlynx on Ice ~~Zoma, Slime~~ | ~~[Appear Strong Enemy](https://players.wotvffbe.com/2640/)~~ | - | - | - | [Interlude - Book of Despair - Section 3](https://players.wotvffbe.com/2637/) | 24/8 | Dragon Quest TACT |
