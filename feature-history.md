## 2022-12-07
* Implement Voice to events
  * The Ephemeral
  * The Agitator
  * The Ancient Tome of Leonis
  * Silence of the Songbirds
* +6 Equipment
  * Gigantaxe (Axe)

## 2022-11-09
* +6 Equipments
  * Alexandrite Ring (Accessory)
  * Diamond Coat (Clothing)
  * Elemental Spear (Spear)
  * Quasar (Gun)

## 2022-10-19
* Backgroun Repeat
  * Story Quests
  * Event Quests
  * Farplane Archives
* +6 Equipments
  * Healing Mace (Mace)
  * Fides Lacerna (Clothing)
  * Flametongue (Sword)
  * Tyrfing (Sword)

## 2022-10-05
* Implement Voice to events
  * The Admirable Prince
  * Threat to the West, Rundall
  * To Burn with Life

## 2022-06-15
* Farplane Archives Update: Add Event Quests
* Add more high-rank league levels (Aries, Taurus, Gemini, Cancer)
* Quick-Pick Match