# Characters
* ~~Deah~~ - Ovis Soldier, Begging Mont to help her country long ago (2-10-2-7)
* ~~Alaya~~ - Sister to Oberon, Betrothed to Jayden (2611)
* ~~Lucielle~~ - Betrothed to Oberon (2611)
* ~~Melnia~~ - Master to Ranell, Ex to Muraga (261A)
* King Roghza - King of Ovis (2614)
* ~~Gernsback~~ [Heindler] - Friend to Resnick (261A)
* ~~Galzahk (Free MR)~~ - Garvall's enemy, former Arms Dealer's Guild head (2625) - Staff
* Lumeide - Comes with Lameiga, Lameiga's sister? (2625) - Staff
* Eldric - In Arms Dealer's Guild, then leave (2626) - Fist
* Daria - Just enfant, daughter to Vinera and Dario Hourne (262A)
* Two mysterious girls, from the Sanctum, may be, invade Hourne Castle in 261A
  * ~~Bronwell~~ - Greatsword
  * Shelva - Mace
  * Not from the Sanctum of Sadali but come to sentence Sadali, instead.
* Lucielle Triplets
  * ~~Shalze~~ - New Costume (3-2-3-10)
  * Rasheena
* Elan and ~~Chunak~~ - Jayden's soldier (2639)
* Cayanne - Minwu's Brother (2315) Dagger
* Grahl - Hourne, Friend to Salire (2614) - Axe
* Barale - Rundall, Subordinate to Rhaldor (2615) - Student of Seymore like Rayches - Sword-like
* ~~Macleod (Free MR)~~ & Valtoa - Ovis First Division << Inviam >> (2631)
* Abbott - Wezette's Captain of << Stiriae >> (263A)
* Ygene - Female Soldier in << Stiriae >> of Wezette (2434), have some relationship with Serjes
* Jurial & ~~Lissette~~ - Lykeros (2318)
* Neia - Protector of Alaya (2-7-2-10)
  * Side with Gouga till found some clues and switch to Saiga
* Jeter - Heindler Guy (2-8-1-10)
* ~~Khamral~~, Jinoch - Lykeros (2-8-3-3)
* Freyser (Book), ~~Sonille~~ (Sword) - Subordinates of Roseluna (3-1-2-1)
* Roseluna (Bow) - Another daughter of Khury (3-1-2-1)
* ~~Exia~~ อยู่ในตู้ใต้โบสถ์ Sadali ( Released on 2024-10-17 )
* ~~Rivelka (Free MR)~~ - ทหารของ Alaya (2-10-2-10)
* ~~Gryfford~~ - หัวหน้า Sonitus of Rundall (2-10-3-6)
* Chancellor Rogaul - Nobel of Wezette น่าจะแค่กี้กี้ (3-1-2-2)
* Garreth - One of Helena's Squad (3-2-3-3)
* Effinger - One of the six fangs (A Bullet and A Vow Ep.6)
* Liv'La, Drace - V Clan - หมู่บ้านเดียวกับ Niv'Lu โดน Raid แล้วมาอยู่กับ Jayden (3-4-1-9)
  * Liv'La เป็น Clan Lady
* Pontiff - High Priest of Tessera ( Homeland of Sadali ) ( Into the Untainted Light Ep.1 )
* Nelsus - Priest work for Pontiff ( Into the Untainted Light Ep.2 )
* Soleius - Son of Macherie and Mont ( 3-11-1-2 )
* ~~Maevia Wezette~~ - Mother of Khury ( To Bloom on the Battlefield Ep.4 )

# Army
## Wezette
* Spirare - Glaciela
* Equito - Serjes
* Stiriae - Abbott
## Leonis
* Fortem - Sterne
* Caelum - Mont
* Lumen - Helena
## Hourne
* 1st Solidus - Engelbert
* 2nd Nitor - Adelard
## Rundall
* 1st Sonitus - Gryfford
* Crucium - Rhaldor
* 3rd Calorum - Lu'Cia
* Vanum - Jayden's personal guard - Elan, Chunak
* Lusttrare - Jayden's personal guard - Liv'La, Drace
## Heindler
* Six Fangs - Gernsback, Resnick, Jeter, Moore, Effinger, Isaac?
## Lykeros
* 2nd: Obtritus - Khamral, Jinoch
