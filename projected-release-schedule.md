# Projected Release Schedule (Global)
My projected release schedule using information from JP event schedule
* [Official WotV FFBE JP Twitter - FFBE幻影戦争 公式](https://twitter.com/WOTV_FFBE)

| GL Date | Units | Upgrade | Vision Cards | 3★ Espers | Raid | Challenge | Story | JP Date | JP Event |
| ---     | ---   | ---     | ---          | ---       | ---  | ---       | ---   | ---     | ---      |
| 19/2 | [Maevia Wezette](https://players.wotvffbe.com/6189/) | **Eq Enh** (Sage's Crimson Plume, Shimmering Lance) | - | - | - | - | [Those of Absent Heart: Part I](https://players.wotvffbe.com/6192/) | 16/1 | - |
| 26/2 | - | **2nd Master** (Veritas of the Waters) **3rd Master** (Amnelis, Velis, Khury Wezette, Duane) **LB Enh** (Helena the Black Rose, Veritas of the Waters) | ["Explosive Patriarch, Dad Bomb", Feast of Fists and Bow](https://players.wotvffbe.com/6205/) | Father Bomb | - | - | 3-12-2 | 23/1 | - |
| 5/3 | [Falsette (Luartha in White)](https://players.wotvffbe.com/6219/) | - | - | - | - | - | - | 1/2 | - |
| 11/3 | - | - | [Falsette Card](https://players.wotvffbe.com/6233/) | - | - | [Raid Boss Revival](https://players.wotvffbe.com/6235/) | - | 6/2 | - |
| 18/3 | [Young Owe](https://players.wotvffbe.com/6244/) | - | [Dark Leviathan](https://players.wotvffbe.com/6244/) | - | - | - | [The Heartless 2](https://players.wotvffbe.com/6250/) | 13/2 | - |
| 25/3 | [Kuja](https://players.wotvffbe.com/6261/) | - | [Kuja's Card](https://players.wotvffbe.com/6261/) | - | - | - | - | 20/2 | FFIX |
| 1/4 | [Moore, Guardian of the Stars](https://players.wotvffbe.com/6274/) | - | - | - | - | - | - | 1/3 | - |
| 8/4 | - | - | Moore, Guardian of the Stars Card | - | [FFIX ToR](https://players.wotvffbe.com/6285/) | - | - | 10/3 | - |

# Released
| GL Date | Units | Upgrade | Vision Cards | 3★ Espers | Raid | Challenge | Story | JP Date | JP Event |
| ---     | ---   | ---     | ---          | ---       | ---  | ---       | ---   | ---     | ---      |
| 7/1 | [Vinera Hourne](https://players.wotvffbe.com/6074/) | - | FFXIV Cards | - | - | - | - | 1/12 | FFXIV |
| 15/1 | - | **2nd Master** (Helena the Black Rose) **3rd Master** (Rain, Glaciela (Flagbearer of Reform)) | [Little Heroes, Big Dreams](https://players.wotvffbe.com/6102/) | - | - | [FFX-2 Guild Raid](https://players.wotvffbe.com/6099/) | 3-11-3 | 10/12 | - |
| - | ~~Perrene (Winter)~~ | - | - | - | - | - | - | 17/12 | - |
| 22/1 | PACK H Refresh, [Gilgamesh (FFV)](https://players.wotvffbe.com/6136/) | **2nd Master** (Bartz, Lenna, Faris) **3rd Master** (Bartz, Lenna, Faris) **LB Enh** (Bartz, Lenna, Faris) | [No one's left behind](https://players.wotvffbe.com/6136/), ~~Winter Card~~ | - | - | - | - | 23/12 | FFV Rerun |
| 29/1 | [~~Hero King Mont~~, Exdeath](https://players.wotvffbe.com/6152/) | **2nd Master** (Oberon the Auspicious) **3rd Master** (Moore the Merrier, Resnick the Hoppy, Oberon the Auspicious) **LB** (Oberon the Auspicious, Astrius the Erudite Bolt) **Equipment** (Purple Dragon Talon) | Exdeath's Return, Duet of Katana and Staff | - | - | - | - | 1/1 | - |
| 6/2 | Hero King Mont | - | - | - | - | - | 3-12-1 | - | - |
| 13/2 | - | **2nd Master** (Oelde, King of the Lions) **3rd Master** (Elsirelle, Raph, Skahal, Fina) **Eq Enh** (Necklace of Compassion) | [Hero King Mont Card](https://players.wotvffbe.com/6177/) | - | [FFV ToR](https://players.wotvffbe.com/6174/) | - | - | 9/1 | - |
