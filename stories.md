# Part 3 Chapter 5 Scene 1
## Battle 1 
* Luartha ตอนอยู่กับ Sadali ชื่อว่า "Sorrow" แต่เธอไม่ชอบชื่อนั้นแล้วนะ

# Side: That Which Must Be Protected
* เรื่องราวของชาวป่าใน Hournes
* Eurel และ Frederika
* Engelbert ได้รับมอบหมายจาก King Robb ให้มาตรวจตราพื้นที่ ว่ามี Fennes บุกมาหรือไม่
* Frederika คือน้องสาวของ Vidd (​ตายไปแล้ว เป็นชาวป่าที่เป็นเพื่อนสนิทของ Engelbert)
* Eurel ตั้งใจเป็นพี่เลี้ยงให้ Frederika รอวันที่เธอพร้อมจะเป็นผู้นำแห่งผืนป่า Rubis

# Part 2 Chapter 10 Scene 3
## Battle 9 : V Clan
* Niv'Lu เข้าห้องลับไปเจอหัวหน้า Clan ที่คิดว่าตายไปแล้ว Liv'La 
* Liv'La กับ Drace (Twilight Tribe) เป็น Clan ที่ถูก Gouga กับ Saladi ไล่ล่า เพื่อป้ายความผิดให้ Rundall
* "Lord Jayden has never laid a finger on me" Liv'La กล่าว
* Liv'La กับ Drace เป็นหน่วยอารักขา Jayden 

## Battle
* Alaya กำลังจะเข้าไปจัดการกับ Gryfford แต่ Gilgamesh บินมาถล่ม Rundall Castle 
* Gryfford ตุ๊ยท้อง Alaya แล้วพาไปหลบในที่ปลอดภัย ( พร้อม Rivelka และ Oldoa )

# Part 2 Chapter 10 Scene 2
## Battle 10 : To Be Free
* Tyrrell คิดว่าจัดการลูกน้องของ Garvall ทั้งหมดได้ แต่นั่นคือลูกน้อง Garvall แกล้งตาย ( มันแกล้งตายเนียนขนาดนั้นเลยเหรอวะ )
* Kilphe (มากับ Tyytas และ Chel) มาช่วย Tyrrell ( อ้อ Tyrrell น่าจะรู้จัก Kilphe ที่ Fennes )
* Kilphe คิดว่า Lykeros น่าจะปลอดภัย
* ตัดมาที่ Rundall Castle: Alaya อยู่กับ Rivelka: Alaya ต้องการไปจัดการกับ Gryfford หัวหน้าทีมลำดับที่หนึ่งของ Rundall โดยมีนินจา Gouga (มั้ง) มาช่วยเหลือ

## Battle 8 : Birthright I
* Garvall บอกให้ฆ่า Macherie แล้วให้ลูกสาว Vinera ขึ้นเป็น Queen of Hourne (Garvall และ Vinera เป็นคน Yezagh)
* ลูกน้องมาส่งข่าว Garvall ว่า Velne ( น้องสาว Garvall ) พา Varush ที่บุกเข้าไปที่ Arms Dealer Guild ไปสักที่ Garvall ให้ลูกน้องคุ้มกันกระท่อมของ Vinera ระหว่างที่ตัวเองไปลุย ( เห็นว่าจะไปหา Sadali เพื่อขอความช่วยเหลือบางอย่าง )
* Tyrrell กะจะแหกลูกน้องทั้งก๊วนออกไป เพื่อพา Vinera หนี

# Part 2 Chapter 9 Scene 3
### Battle 5 : In King Oberon's Hands I
* Resnick ปลอดภัยดีแล้ว Jeter มาแจ้งข่าวกับ Gernsback
### Battle 4 : Wariness
* Bronwell & Shelva เจอ Exia อยู่ในตู้น้ำขนาดใหญ่ใต้โบสถ์ Sadali

# Part 2 Chapter 8 Scene 2
## Battle 4 : Power of the Relic I
* Gargas & Dorando เจอ Nador อยู่ในขบวนชาวบ้านของ Sadali
* Nador แทง Gargas ด้วย Ancient Relic ที่ดึงออกมาจากตัว Dorando
* Dorando ทุบ Nador ให้สลบแล้วพาตัวไป
* Dorando เอามีด Ancient Relic ออกจากตัว Gargas ก็ฟื้นทันทีแบบไม่รู้สึกบาดเจ็บอะไร
## Battle 1 : Brainwashed I
* Bronwell กับ Shelva คุยถึง Sadali ว่ามาพร้อมกับผู้หญิงคนหนึ่งชื่อ Exia ( เหมือนจะเคยเห็น Sadali หรือใครสักคน พูดถึง Exia มั้ยนะ? )
* สองสาวเจอขบวนของคนจาก Hourne ที่โดน Sadali สะกด คุยกันอยู่ดี ๆ ก็โดนขบวนโจมตีใส่
* Bronwell นี่ใช้ดาบใหญ่ด้วยนะ ส่วน Shelva น่าจะเป็น Rod หรือ Mace

# Part 2 Chapter 8 Scene 1
## Battle 6 : Change of Plans
* หลังจาก Jayden ยิงป้อม Sadali ซะแหลก Lu'Cia ตัดสินใจตรึง Melnia ให้สู้กับ Calorum ต่อไป เพื่อเป็นประโยชน์กับ Jayden ในการไล่ล่า Sadali
## Battle 5 : The Ordeal of Treatment
* Gilgamesh เข้ามาขวางแล้วทำให้ Sterne สลบไปก่อน เพื่อเตรียมล้างพลัง Dark ของ Sadali ที่ยังติดอยู่ในตัว Sterne
## Battle 3 : Gloom Skeletons I
* Seymore พา Sterne ไป Ariale Castle แล้ว Sterne เกิดคุ้มคลั่งจาก ออร่า Dark อะไรซักอย่าง พุ่งเข้าใส่ the Deity ( Vessel for Oracle )

# Part 2 Chapter 7 Scene 3
## Battle 1 : 
* Lu'Cia โดน Abbot เห็นและยิงปืนใหญ่ใส่ แต่ก็ยังบุกต่อ

# Part 2 Chapter 7 Scene 2
## Battle 10 : 
* Livialle ตบหัว Ahlim ดังป้าบบบบบบ โทษฐาน ปากโป้งดีนัก
* Elan ให้ Neia, Livialle, Ahlim รีบกลับไปส่งข่าวที่ Rundall Castle แล้วฉายเดี่ยวกับปาตี้ของ Mariale
## Battle 1 : The Only One I Can Ask I
* Jayden ให้ Elan กลับไปป้องกัน Rundall Castle

# Part 2 Chapter 7 Scene 1
## Battle 1 : For Our Son I
* Sadali กำลังจะเดินแผนการขั้นต่อไป
* Dorando & Gargas กำลังกลับไปหา Nador ( น่าจะเป็นเด็กที่เลี้ยงไว้ )

# Part 2 Chapter 6 Scene 3
## Battle 10 : When Ruin Shall Befall Them
* Sadali รอเวลาให้ when ruin shall befall them ( Rundall )
* Abbott ( หัวหน้าหน่วยเรือของ Wezette ) มาเข้ากับ Sadali
* ตัวละครหญิงในตำนาน () ของวัง Gilgamesh ( Ariale Castle ) โผล่มาแจ้งเตือน ว่ามีอันตรายกำลังเข้าวัง แล้วภาพก็ตัดไปที่ Sterne กำลังเดินไปหา Gilgamesh (​เพื่อถามเรื่องที่อยู่ในสมุดบันทึกนั่นหละ)
## Battle 9 : The Greatest of Stages
* Elan บ่นอยากไปทำแทน Oberon
* Jayden เตรียม The Greatest of Stages ให้กับ Elan & Chunak
  * ไปสู้กับ Sadali นั่นเอง
## Battle 7 : Right Here where I Stand I
* Resnick เสียท่าให้ Vision ของ Shalze โดนฟันไป 1 ดอก
* Gernsback โดดฟาดตายสามตัว
* Resnick ขอให้ Gernsback ฆ่าตัวเอง ( kill me right here where i stand ) เพื่อให้เกิด conflict ระหว่าง Jayden กับ Oberon ( Resnick เป็นหนึ่งใน Six Fangs ของ Heindler )
* แต่ Gernsback ไม่ทำ เพราะอยากให้ Oberon ตัดสินใจเอง และทั้งสองจะต้องอยู่เคียงข้าง Oberon ต่อไป
## Battle 6 : Time and Time Again
* Oberon น้อยใจ Jayden ที่ทดสอบตัวเองอยู่เรื่อยไป ( time and time again ) ไม่ไว้ใจ Oberon เหรอ?
## Battle 5 : The Right Decision
* ทัพ Oberon มาถึงหน้า Ovis Castle และ Shalze หนีออกมาพอดี แต่ Oberon ก็รู้ได้ทันทีว่านี่ไม่ใช่ Lucielle คนรักของเค้า Oberon ไล่ตามจับ Shalze
## Battle 4 : The Bearer of the Ring
* Lucielle ตัวปลอม คือ Shalze ( หนึ่งในแฝดสามของ Lucielle )
  * Lucielle
  * Shalze
  * Rasheena
* Shalze บอกให้อีกสองคนหนีไป เพื่อให้ตัวเองทำหน้าที่ของ Royal Family
  * เอาแหวนจาก Lucielle มาใช้ เพื่ออำนาจของตัวเอง
## Battle 1 : Eternal Rest I
* ทหาร Leonis พา Mont เข้า Ovis Castle
* Lucielle ( ตัวปลอม ) พยายามจะชิงแหวนของ Mont
* Macleod & Valtoa ปรากฏตัวเพื่อขัดขวาง
* King Roghza is a monster

# Part 2 Chapter 6 Scene 2
## Battle 10 : Daria
  * Tyrrell โดน Garvall ตามตัวได้ ทำให้มาเจอ Vinera พร้อมลูกสาว (Daria) ทำให้ Garvall คิดแผนใหญ่จะฮุบ Hourne มาไว้เป็นของชาว Yezagh ( Vinera, Garvall และ Velne เคยอยู่ในค่ายเดียวกัน ก่อนที่ Vinera จะถูก Muraga ชุบตัวและส่งไปแต่งงานกับ Dario )
  * Lucielle น่าจะโดน Sanctum ลักพาตัวไปอยู่ที่ Arms Dealer's Guild และส่งตัวปลอมไปไว้ที่ Ovis ( ชื่อ Shalze ? )
    * Guild โดนบุก Lumeide และ Lameiga ( Eldric ด้วย ) พาตัว Lucielle ไปไว้ที่ Sanctum 
    * มิน่า Lucielle ที่ Ovis ดูจะเป็นทาสของ Sadali

# Part 2 Chapter 3 Scene 3
## Battle 5 : Luathar's Past
* แท้จริงแล้ว Luathar เป็น Homunculus ที่ Sadali สร้างขึ้นมีเหมือนสามพี่น้องนั่นแหละ แต่ Luathar หนีออกมา
