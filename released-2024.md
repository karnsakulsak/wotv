# Released
| GL Date | Units | Upgrade | Vision Cards | 3★ Espers | Raid | Challenge | Story | JP Date | JP Event |
| ---     | ---   | ---     | ---          | ---       | ---  | ---       | ---   | ---     | ---      |
| 2/1 | Astrius the Erudite Bolt | - | - | - | - | - | - | 1/10 | - |
| 10/1 | - | **Transcendence** (Esther, Ibara, Sylvie, Shadowlynx on Ice) **2nd Master** (Esther, Ibara, Sylvie, Shadowlynx on Ice) **LB Enh** (Fryevia, Luartha, Glaciela Wezette, Oberon Heindler) **Equipment +1** (Esther, Ibara, Sylvie, Shadowlynx on Ice weapons) | "Dragon of Ten Thousand Thunders, Voldiskke", Hollow Vessel | Voldiskke | Ultros | Legendary Reliquaries - Greatsword, Gun | 3-6-2 | 10/10 | - |
| 17/1 | [Bronwell, Khamral - Free MR](https://players.wotvffbe.com/4178/) | - | [Elsirelle Solo Mission](https://players.wotvffbe.com/4193/) | - | - | - | AS3-2, AS3-3 | 15/6, 22/6 | - |
| 24/1 | A2 - NieR, Veritas of the Heavens | **Transcendence** (A2, 2B, 9S) **2nd Master** (2B, 9S) **LB Enh** (2B, 9S, Yerma, Skahal, Alaya Rundall) **Equipment** (Blood Sword, Genji Glove, Curel Arrogance, Pod 153, Virtuous Contract) | A2 the Deserter, A Watched Pot Boils | - | - | - | - | 22/12 | - |
| 1/2 | 2P - NieR, "Lucio, Light of Brilliance" | **Transcendence** (2P, Lucio LoB) **LB Enh** (Helena BRW, Macherie QoH) | New Year's Banquet | - | - | - | 3-6-3 | 1/1 | - |
| 8/2 | Oberon the Auspicious | **Transcendence** (Oberon tA, Moore tM, Resnick tH) **2nd Master** (Resnick tH) **LB Enh** (Moore tM, Resnick tH, Macherie, Kitone, Oldoa) **Equipment** (Itty-bitty Bunny Blade) | Between Hope and Despair (Lucio LoB), Perfect Production Line (Free) | - | NieR Raid | - | - | 10/1 | - |
| 15/2 | [Shadowlynx the Crystalborn](https://players.wotvffbe.com/4985/) | - | A Blessed Sun Rises (Oberon tA Card) | - | - | - | AS3-4 | 17/1 | - |
| 22/2 | [Veritas of the Waters, Lunacestus Kitone](https://players.wotvffbe.com/5012/) | **Transcendence** (Veritas of the Heavens, Lunacestus Kitone) **LB Enh** (Rhaldor, Eliza, Niv'Lu) **Equipment** (Sharpshooter's Monocle, Yoichi's Bow, Murasame, Tide Ring) | [Fated Confrontation](https://players.wotvffbe.com/5013/) | - | - | Raid Boss Revival | 3-7-1 | 24/1 | - |
| 1/3 | Dario the Crystalborn | **Transcendence** (Dario the Crystalborn) **2nd Master** (Rysol) | - | - | - | - | - | 1/2 | - |
| 8/3 | - | **2nd Master** (Sterne (Wing of Destiny), Raegen, Miranda (Sweetheart)) **LB Enh** (Lu'Cia, Vinera, Glaciela (Regalia), Salire (Sweetheart), Ildyra (Sweetheart), Miranda (Sweetheart)) | [Trove of Knowledge](https://players.wotvffbe.com/5061/) | - | - | Spire of Wind | - | 7/2 | - |
| 15/3 | [Fang - FFXIII](https://players.wotvffbe.com/5127/) | **Transcendence** () | - | - | - | - | AS3-5 | 14/2 | - |
| 22/3 | - | - | Fang Card | - | - | - | - | 21/2 | - |
| 1/4 | Strategist Oldoa, Jeume the Spring Celestial | - | Jeume Card (Free) | - | - | - | AS3-6 | 1/3 | - |
| 10/4 | - | - | Strategist Oldoa Card | - | FFXIII ToR | Limited Arena | 3-7-3 | 8/3 | - |
| 17/4 | Volke | - | - | - | - | - | - | 15/3 | - |
| 24/4 | Veritas of the Earth | - | Volke Card | - | - | - | - | 25/3 | - |
| 1/5 | Ashen King Mont, ~~Jeume the Spring Celestial~~ | - | - | - | - | - | 3-8-1 | 1/4 | - |
| 8/5 | - | - | [Dark Titan](https://players.wotvffbe.com/5302/), Card from Raid | Dark Titan | [Dark Ifrit](https://players.wotvffbe.com/5300/) | - | [AS3-7](https://players.wotvffbe.com/5308/) | 9/4 | - |
| 15/5 | [Cloud - FFVIIAC, Tifa - FFVIIAC - Free](https://players.wotvffbe.com/5361/) | - | [Cloud FFVIIAC](https://players.wotvffbe.com/5361/) | - | - | - | - | 16/4 | - |
| 22/5 | [Kadaj - FFVIIAC](https://players.wotvffbe.com/5379/) | - | [Kadaj FFVIIAC](https://players.wotvffbe.com/5379/) | - | - | - | - | 23/4 | - |
| 3/6 | Sefiroth - FFVIIAC | - | - | - | - | - | - | 1/5 | - |
| 12/6 | - | - | Sefiroth - FFVIIAC | - | - | - | - | 9/5 | - |
| 19/6 | Heaven Blade Gilgamesh | - | - | - | - | - | 3-8-3 | 16/5 | - |
| 26/6 | [Veritas of the Fire](https://players.wotvffbe.com/5458/) | - | [Chariot](https://players.wotvffbe.com/5458/) | - | - | Raid Boss Revival | - | 23/5 | - |
| 5/7 | [Lucielle Ovis](https://players.wotvffbe.com/5478/) | - | Ovis' Demon Princess | - | - | - | - | 1/6 | - |
| 12/7 | [Seifer - FFVIII](https://players.wotvffbe.com/5502/) | **2nd Master** (Squall, Rinoa, Irvine, Jayden the Celebrated, Alaya the Alabaster) **LB Enh** (Squall, Rinoa, Irvine, Elsirelle, Little Leela the Bold, Howlet the Heavensblade) **Equipment** (Shooting Star, Lion Heart) | - | - | - | - | 3-9-1 | 10/6 | - |
| 22/7 | - | - | Seifer Card | - | - | FFVIII ToR | - | 17/6 | - |
| 30/7 | [White Lotus Fina, Veritas of the Ice](https://players.wotvffbe.com/5525/) | - | White Lotus Fina Card | - | - | - | - | 24/6 | - |
| 7/8 | Resnick (Summer) | **2nd Master** (Jayden (Summer), Glaciela (Summer)) **LB Enh** (Jayden (Summer), Glaciela (Summer)) | Resnick (Summer) Card | - | - | - | 3-9-2 | 1/8 | - |
| 14/8 | [Sonille (UR - Free)](https://players.wotvffbe.com/5560/) ~~[Shurecca](https://players.wotvffbe.com/5540/)~~ | - | [Orochi](https://players.wotvffbe.com/5624/) | [Orochi](https://players.wotvffbe.com/5624/) | - | - | - | 1/7 | - |
| 21/8 | PACK H Refresh ~~[Sonille (UR - Free)](https://players.wotvffbe.com/5560/)~~ [Shurecca](https://players.wotvffbe.com/5540/) | - | ~~[Garuda Card](https://players.wotvffbe.com/5560/)~~ | ~~[Garuda](https://players.wotvffbe.com/5560/)~~ | - | - | - | 9/7 | - |
| 28/8 | Veritas of the Bolt, ~~[Ramada, Maiden of the Stars](https://players.wotvffbe.com/5577/)~~ | - | [Garuda Card](https://players.wotvffbe.com/5560/) | - | - | - | 3-9-3 | 17/7 | - |
| 4/9 | ~~Veritas of the Bolt~~, [Ramada, Maiden of the Stars](https://players.wotvffbe.com/5577/) | - | ~~[Ramada, Maiden of the Stars Card](https://players.wotvffbe.com/5595/)~~ | - | - | ~~[Guild Raid](https://players.wotvffbe.com/5591/)~~ | - | 24/7 | - |
| - | ~~Resnick (Summer)~~ | - | ~~Resnick (Summer) Card~~ | - | - | - | - | ~~1/8~~ | - |
| 11/9 | - | - | ~~[Orochi Card](https://players.wotvffbe.com/5624/)~~, , [Ramada, Maiden of the Stars Card](https://players.wotvffbe.com/5595/) | ~~[Orochi](https://players.wotvffbe.com/5624/)~~ | - | [Tower of Fire](https://players.wotvffbe.com/5626/) | - | 8/8 | - |
| 19/9 | PACK H Refresh, [Lilyth, Knight of Crimson Conviction](https://players.wotvffbe.com/5639/) | - | [Resisting Despair - Lilyth, Knight of Crimson Conviction Card](https://players.wotvffbe.com/5639/) | - | - | - | 3-10-1 | 15/8 | - |
| 26/9 | Duran, Angela | - | Trials of Mana, Reinforcements from Another World | - | - | - | ~~3-10-1~~ | 22/8 | ToM Collab |
| 3/10 | [Crimson Wizard, Sadali the Wayfaring Priest](https://players.wotvffbe.com/5766/) | - | [Seeker of Power](https://players.wotvffbe.com/5757/) | - | - | - | - | 1/9 | - |
| 10/10 | - | Day of Visions | [For the Unforeseen Future - Sadali the Wayfaring Priest Card](https://players.wotvffbe.com/5776/) | - | - | [ToM ToR](https://players.wotvffbe.com/5775/) | 3-10-2 | 10/9 | - |
| 17/10 | PACK H Refresh, Alaya (Halloween), [Exia](https://players.wotvffbe.com/5792/) | **2nd Master** (Lucio (Halloween)) **LB Enh** (Lucio (Halloween)) **Equipment** (Rosethorn, Fomalhaut) | - | - | - | - | - | 17/9 | - |
| 24/10 | Veritas of the Light | - | Halloween in the Starlit Sky - Alaya (Halloween) Card, [Together in Eternity - Exia Card](https://players.wotvffbe.com/5812/) | - | - | - | - | 24/9 | - |
| 1/11 | [Garrousa](https://players.wotvffbe.com/5824/) | **Transcendence** (Agrias, Delita, Orlandeau, Ramza, Mustadio, Gafgarion) **LB Enh** (Agrias, Delita, Orlandeau, Ramza) | - | - | - | - | 3-10-3 | 1/10 | FFT Rerun |
| 8/11 | - | - | [King of Instant Needles, Jumbo Cactuar](https://players.wotvffbe.com/5855/), Remaining in Ruin | Jumbo Cactuar | [Dark Golem Raid](https://players.wotvffbe.com/5851/) | - | - | 9/10 | - |
| 14/11 (JP 5Y) | PACK H Refresh, Jayden, King of Conquest ~~Alaya (Halloween)~~ | - | - | - | - | - | 3-11-1 | 16/10 | - |
| 22/11 | Veritas of Darkness, Nicole | - | Nicole Card, ~~Halloween in the Starlit Sky~~ | - | - | - | - | 23/10 | - |
| 2/12 | Paine, Yuna (FFX-2) | - | YRP, Taking the Stage! | - | - | - | - | 1/11 | FFX-2 |
| 10/12 | Rikku (FFX-2) | **3rd Master** (Alaya the Alabaster, Kitone, Alaya Rundall, Helena (Black-Robed Witch), Starlight Elena) | Gullwings' Merry Maker | - | - | - | - | 8/11 | - |
| 17/12 | PACK H Refresh, Perrene (Winter), ~~Jayden, King of Conquest~~ | **2nd Master** (Raph (Winter)) **3rd Master** (Ramada, Macherie, Vinera, Viktora, Luartha, Raviesse, Raph - Winter) **LB Enh** (Raph (Winter)), ~~**3rd Master** (Alaya the Alabaster, Kitone, Alaya Rundall, Helena (Black-Robed Witch), Starlight Elena)~~ | - | - | - | - | 3-11-2 | 14/11 | JP 5Y |
| 24/12 | - | - | Paine Card, Winter Card | - | - | - | - | 22/11 | - |

